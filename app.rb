require 'sinatra'
require 'haml'
require 'redis_orm'
require 'json'
require 'sinatra/content_for'
require 'sinatra/formkeeper'

set :protection, except: :ip_spoofing

configure :production do
	 #Appfog Settings
   #services = JSON.parse(ENV['VCAP_SERVICES'])
   #  redis_key = services.keys.select { |svc| svc =~ /redis/i }.first
   #  uri = services[redis_key].first['credentials']
	 #  $redis = Redis.new(:host => uri['hostname'], :port => uri['port'], :password => uri['password'])
	 
	 #Heroku Settings
	 uri = URI.parse(ENV["REDISTOGO_URL"])
	 $redis = Redis.new(:host => uri.host, :port => uri.port, :password => uri.password)
end

configure :development do
  $redis = Redis.new(:host => "localhost", :port => "6379")
end

class Auction < RedisOrm::Base 	
	property :email, string
	property :name, string
	property :picture_path, string
	property :google_map_url, string
	property :description, string
	property :start_bid, string 
	property :bid_guid, string
	property :auction_guid, string
	has_many :bids

	index :bid_guid
	index :auction_guid
	index :email

	def high_bid
		high_bid = start_bid.to_f
		bids.each do |bid|
			high_bid = bid.value.to_f if bid.value.to_f > high_bid
		end
		return high_bid
	end

end

class Bid < RedisOrm::Base 	
	property :name, string 
	property :email, string 
	property :value, integer
	belongs_to :Auction
end


form_messages File.expand_path(File.join(File.dirname(__FILE__), 'form_error_messages.yaml'))

get '/' do
  haml :auction_picture
end

not_found do 
	haml :notFound
end

post '/auction_description' do
	form do 
		field :photo, :present => true
    end

	if form.failed?
        haml :auction_picture
    else
    	tmpfile = form[:photo][:tempfile]
    	filename = form[:photo][:filename]
    	localpath = File.join('./public/auction_resources/', filename )

    	File.open(localpath, 'w') do |f|
        	f.write tmpfile.read
      	end
      	bid_guid = SecureRandom.urlsafe_base64
        auction_guid = SecureRandom.urlsafe_base64
      	@auction = Auction.create(:picture_path => "auction_resources/#{filename}", :bid_guid => bid_guid, :auction_guid => auction_guid)
    	haml :auction_description
    end
end

post '/auction_price' do
	form do 
		field :description, :present => true 
    end
    @auction = Auction.find_by_auction_guid params[:auction_guid]
    if form.failed?
        haml :auction_description
    else
    	@auction.description = form[:description]
    	@auction.save
    	haml :auction_price
    end

end

post '/add_contacts' do 
	form do 
		field :bidvalue, :present => true
		field :start_bid, :present => true 
    end
    @auction = Auction.find_by_auction_guid params[:auction_guid]
    if form.failed?
        haml :auction_price
    else
    	@auction.start_bid = form[:start_bid]
    	@auction.save
    	haml :auctiongenerated
    end

end 

get '/bid/:bid_page_guid' do
  @auction = Auction.find_by_bid_guid params[:bid_page_guid]
	@auction_high_bid = @auction.high_bid
  haml :bid	
end

post '/bid/:bid_page_guid' do
  @auction_details = Auction.find_by_bid_guid params[:bid_page_guid]
  form do 
  	filters :strip
  	field :name, :present => true, :length => 4..100
 	field :email, :present => true, :length => 5..100
 	field :value, :present => true
  end
  if form.failed? 
  	haml :bid
  else
	@bid_details = Bid.create(:name => form[:name], :email => form[:email], :value => form[:value], :auction_guid => @auction_details.auction_guid)
	@auction_details.bids << @bid_details
	haml :bidsubmitted	
  end
end


get '/admin/:auction_page_guid' do
  @auction = Auction.find_by_auction_guid params[:auction_page_guid]
  haml :admin 
end

